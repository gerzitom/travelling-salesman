//
// Created by Tomáš Geržičák on 25.01.2021.
//

#include <vector>
#include <ostream>
#include "string"
using namespace std;

#define MAX 9999

#ifndef TRAVELLING_SALESMAN_TRAVELLINGSALESMANPROBLEM_HPP
#define TRAVELLING_SALESMAN_TRAVELLINGSALESMANPROBLEM_HPP


class TravellingSalesmanProblem {
private:
    int n;
    vector<vector<int>> distan;
    int completed_visit;
    vector<vector<int>> DP;
    int begin;

    vector<int> path;
    int pathLength;

    int TSP(int mark, int position);

    void computePath(int mask,int pos);

public:
    TravellingSalesmanProblem(int n, vector<vector<int>> distan) : n(n), distan(std::move(distan)){
        completed_visit = (1<<n) -1;
        begin = 0;
        for(int i = 0; i < (1<<n); i++){
            DP.push_back(vector<int>(n));
        }
        computeTSP();
        computePath(1, begin);
    }

    void computeTSP();

    int getPathLength(){
        return pathLength;
    }

    void printPath(const vector<string> cityNames, ostream &cout){
        cout << "Travelling salesman path: \n";
        cout << cityNames[begin] << " -> ";
        for(int city : path){
            cout << cityNames[city] << " -> ";
        }
        cout << cityNames[begin];
    }
};


#endif //TRAVELLING_SALESMAN_TRAVELLINGSALESMANPROBLEM_HPP
