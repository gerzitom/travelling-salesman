//
// Created by Tomáš Geržičák on 25.01.2021.
//
#include "string"
#include <vector>
#include <sstream>

#ifndef TRAVELLING_SALESMAN_CMD_DATA_HPP
#define TRAVELLING_SALESMAN_CMD_DATA_HPP
using namespace std;

class cmd_data{
public:
    vector<string> cityNames;
    vector<vector<int>> graph;
    int graphSize;

    void parse_data(istream &cin);
private:
    std::vector<std::string> split(const std::string& s, char delimiter);
};

#endif //TRAVELLING_SALESMAN_CMD_DATA_HPP
