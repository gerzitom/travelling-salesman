#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include <sstream>
#include "string"
#include "cmd_data.hpp"
#include "TravellingSalesmanProblem.hpp"

using namespace std;
#define MAX 9999


bool is_help(std::string const& argument) {
    return argument == "--help" || argument == "-h";
}

void print_help(){
    FILE* fp = fopen("help.txt", "r");
    if (fp == NULL) exit(EXIT_FAILURE);

    char* line = NULL;
    size_t len = 0;
    while ((getline(&line, &len, fp)) != -1) {
        // using printf() in all tests for consistency
        printf("%s", line);
    }
    fclose(fp);
    if (line) free(line);
}

int main(int argc, char** argv) {

    if (argc == 2) {
        // parse options
        if (std::any_of(argv, argv+argc, is_help)) {
            print_help();
            return 0;
        } else {
            std::clog << "Bad argument entered.\n";
            return 1;
        }
    } else if (argc >= 3) {
        std::clog << "Too many arguments given.\n";
        return 1;
    }

    cmd_data cmdData;
    try{
        cmdData.parse_data(cin);
    } catch (const char* msg) {
        clog << msg;
        return 1;
    }

    auto tsp = TravellingSalesmanProblem(cmdData.graphSize, cmdData.graph);
    tsp.printPath(cmdData.cityNames, cout);

    cout << "\nComplete path length is: " << tsp.getPathLength() << "\n";

    cout << "\n";
    return 0;
}
