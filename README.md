# Travelling salesman problem
Semestrální práce do předmětu PJC.

## Zadání
Úkolem bylo naiplmentovat řešení pro Travelling salesman problem.

## Použití
Tento program přebírá vstup z konzole podle paradigmatu, které bude popsáno níže.

### Argumenty
| Argument   |  Popis  |
|------------|---------|
| -h         | Vypíše nápovědu pro program

### Struktura dat
Data pro program musí být v tomto formátu:
* Na prvním řádku jsou Jména měst.
* Na dalších řádcích je graf cen cest mezi městy.

Příklady pro program jsou formulovány v souborech ve složce `example_files`.

## Implementace
Projekt je ve standartu C++ 14. Je to pouze jednojádrová aplikace. Nemá žádné externí závislosti..

Problém je řešen metodou Dynamic programming.

Program má přepínač `-h` pro vypsání nápovědy, tento text může být upraven v souboru 

Projekt je rozdělen do 2 částí:
* cmd_data (cmd_data.hpp, cdm_data.cpp)
* TravellingSalesmanProblem (TravellingSalesmanProblem.hpp, TravellingSalesmanProblem.cpp)

### cmd_data
Tato třída zpracovává data. Hlavně názvy měst a graf se spojenímy mezi městy.
Tyto data jsou potom vystaveny, jako public proměnné pro výpočet.

### TravellingSalesmanProblem
Tato třída se stará o hlavní výpočet problému. Bere si data ze třídy cmd_data.
Dokáže počítat cestu v grafu a také cenu celé cesty.


## Příklady
Příklady jsou ve složce `example_files`.

| Soubor      | Počet měst | Předpokládaná cesta
| ------------|------------|------------------------
| example1    | 6          | LA -> Chicago -> New Orleans -> Dallas -> San Francisco -> NY -> LA
| example2    | 4          | LA -> Chicago -> Dallas -> NY -> LA
| example3    | 4          | LA -> Chicago -> Dallas -> NY -> LA