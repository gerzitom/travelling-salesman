//
// Created by Tomáš Geržičák on 25.01.2021.
//

#include "cmd_data.hpp"

void cmd_data::parse_data(istream &cin) {
// Create a text string, which is used to output the text file
    string myText;

    // get city names
    getline (cin, myText);

    cityNames = split(myText, ',');

    graphSize = cityNames.size();

    size_t i = 0;
    // parse graph
    while (getline (cin, myText)) {
        vector<string> graphLine = split(myText, ',');
        size_t y = 0;
        graph.push_back(vector<int>(graphSize));
        for(string number : graphLine){
            graph[i][y] = stoi(number);
            y++;
        }
        if(y != graphSize) throw "Parsing error";
        i++;
    }
}

std::vector<std::string> cmd_data::split(const string &s, char delimiter){
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (std::getline(tokenStream, token, delimiter)){
        tokens.push_back(token);
    }
    return tokens;
}
