//
// Created by Tomáš Geržičák on 25.01.2021.
//

#include "TravellingSalesmanProblem.hpp"

void TravellingSalesmanProblem::computeTSP() {
    /* initialize the DP array */

    for(int i=0;i<(1<<n);i++){
        for(int j=0;j<n;j++){
            DP[i][j] = -1;
        }
    }
    pathLength = TSP(1,begin);
}

void TravellingSalesmanProblem::computePath(int mask, int pos) {
    if(mask==completed_visit) return;
    int ans = INT_MAX, chosenCity;

    for(int city=0;city<n;city++){

        if((mask&(1<<city))==0){

            int newAns = distan[pos][city] + DP[mask|(1<<city)][city];
            if(newAns < ans){
                ans = newAns;
                chosenCity = city;
            }
        }

    }
    path.push_back(chosenCity);
    computePath(mask|(1<<chosenCity),chosenCity);
}

int TravellingSalesmanProblem::TSP(int mark, int position) {
    if(mark==completed_visit){      // Initially checking whether all
        // the places are visited or not
        return distan[position][0];
    }
    if(DP[mark][position]!=-1){
        return DP[mark][position];
    }
    //Here we will try to go to every other places to take the minimum
    // answer
    int answer = MAX;
    //Visit rest of the unvisited cities and mark the . Later find the
    //minimum shortest path
    for(int city=0;city<n;city++){
        if((mark&(1<<city))==0){
            int newAnswer = distan[position][city] + TSP( mark|(1<<city),city);
            if(newAnswer < answer){
                answer = newAnswer;
            }
//                answer = min(answer, newAnswer);
        }
    }
    return DP[mark][position] = answer;
}
